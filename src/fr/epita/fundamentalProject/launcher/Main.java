package fr.epita.fundamentalProject.launcher;


import java.util.InputMismatchException;

import java.util.Scanner;

import fr.epita.fundamentalProject.data.Pdf;
import fr.epita.fundamentalProject.data.Quiz;
import fr.epita.fundamentalProject.data.User;

/***
 *  Launcher of the app
 *  
 *  @author Emmanuel Bentum
 *  @version 1.O
 *  @since 1.0
 * 
 */

public class Main {
	
	/**
	 *  The user operating System name
	 */
	public static String osComplet = System.getProperty("os.name").toLowerCase();
	public static String[] osSplitted = osComplet.split(" ");
	public static String OS = osSplitted[0];
	
	/**
	 * the user personal directory
	 */
	public static String homeDirectory=System.getProperty("user.home");

	
	/** 
	 * Main program
	 * @param args : argument of the main program
	 * @throws InterruptedException if the thread sleep for animation just before launching the quiz gets interrupted
	 */
	public static void main(String[] args) throws InterruptedException {
		
		
			Scanner sc = new Scanner(System.in);
			String level,topic,name, levelInString ,plainFormat ;
			StringBuilder stringBuilderBegining = new StringBuilder();
			
			int levelInt=0, topicInt=0 ,who=0, adminChoice=0, adminChoiceCategory=0;
			
 			
			
			
			
			
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n+\t\t\t\t\t\t\t\t\t\t\t+\n+ \t\t\t\t  WELCOME TO QUIZ MANAGER\t\t\t\t+\n+\t\t\t\t\t\t\t\t\t\t\t+\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println();
		stringBuilderBegining.append("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n+                                                                                                                                                 +\n+                                                          QUIZ MANAGER                                                           +\n+                                                                                                                                                 +\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n");
		
	do {
		System.out.print("Are you an administrator(1) or a user (2) ?  or you want to exit (3): ");
		who = sc.nextInt();
		
		if (who == 1) {
			
			System.out.println("\n\n what do you want to do :\n"
					+ "1)Add a question-answer\n"
					+ "2)list the questions-answers of a subject\n"
					+ "3)Modify either a question or an answer\n"
					+ "4)Exit");
			adminChoice=sc.nextInt();
			System.out.println("Specify the Category");
			switch(adminChoice)
			{
				case 1: 
						
				break;
				case 2: break;
				case 3: break;
				case 4: break;
			}
			
		}
		if (who == 2) {
					do {
						System.out.print("Enter your username : ");
						name=sc.nextLine();
					}while(name.isEmpty());
					stringBuilderBegining.append("Name: "+name+"\n");
					System.out.println("\n");
					System.out.println("Choose a topic : ");
					
					do {
						
						System.out.println("  1.Java \n  2.Logic! \n  3.Geography  ");
						System.out.print("============> ");
						topic=sc.nextLine();
						
						System.out.println();
						try {
							topicInt = Integer.parseInt(topic);
						}catch( InputMismatchException e) {
							System.out.println("Please you need to enter a number either 1, 2 or 3");
						}
						catch( NumberFormatException e) {
							System.out.println("Please you need to enter a number either 1, 2 or 3");
						}
					}while(!topic.equals("1") && !topic.equals("2") && !topic.equals("3") );
					
					if (topic.equals("1"))
						stringBuilderBegining.append("Topic: Java\n");
					else if(topic.equals("2"))
						stringBuilderBegining.append("Topic: Logic\n");
						else
							stringBuilderBegining.append("Topic: Geography\n");
					
					do {
						
						System.out.println("Choose Difficulty \n  1.easy \n  2.medium \n  3.hard ");
						System.out.print("============> ");
						
						level = sc.nextLine();
						try {
							 levelInt = Integer.parseInt(level);
						}catch (Exception e) {
							System.out.println("Please you need to enter a number either 1, 2 or 3");
						}
						
					}while(!level.equals("1") && !level.equals("2") && !level.equals("3"));
					
					System.out.println("\n");
					
					if (levelInt==1) {
						levelInString="easy";
					stringBuilderBegining.append("Level: Easy\n\n\n");
					}
					else if(levelInt==2) {
						levelInString="medium";
						stringBuilderBegining.append("Level: Medium\n\n\n");
					}
					else {//System.out.println("OS: "+ OS);
						levelInString="hard";
						stringBuilderBegining.append("Level: Hard\n\n\n");
					}
					
					//************* USeless Code, just for the purpose of a little animation**********************************
					 
					System.out.print("Generating Quiz ");
					
						for (int k=0; k<3;k++)
						{	Thread.sleep(500);
							System.out.print(".");
							
						}
					
					//****************************************************************************************************
					
					System.out.println("\n\n==============================================================================================\n");
					
					
					/**
					 * @see Quiz 
					 */
					Quiz quiz = new Quiz(topicInt,levelInString,stringBuilderBegining); // create an instance of Quiz
					quiz.launch(); //  launch the quiz
					
					User user = new User(name); // create an instance of User to save his Score
					user.setScore(quiz.getScore()); // save the score of the user
					
					System.out.println("\n"+ user.getName()+", you get a score of " + user.getScore()+" over 10");
					quiz.showCorrection();
					
					plainFormat=quiz.retrievePlainText(); 
					
					Pdf filePdf = new Pdf(plainFormat);
					
					
					filePdf.exportToPdf();
					
					sc.close();
		
		
	}
		
		if ( who == 3) System.exit(0);
		
	}while( who != 1 || who != 2 );
	

}
}
