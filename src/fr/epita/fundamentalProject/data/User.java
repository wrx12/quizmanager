package fr.epita.fundamentalProject.data;

/**
 * 
 * this Class represent a User .
 * a User is defined by his name and his score
 * 
 * @author Emmanuel Bentum
 * @version 1.0
 * @since 1.0
 *
 */
public class User {

	private String name;
	private int score;
	
	/**
	 * Constructor of class User
	 * 
	 * @param name :
	 * 				the name of the user 
	 */
	public User( String name) {
		this.name=name;
	}
	
	/**
	 * the getter of  attribute name
	 * 
	 * @return a String representing the name
	 */
	public String getName() {
		return this.name;
	}
	/**
	 * Setter of attribute name
	 * 
	 * @param n the new name of the user
	 */
	public void setName(String n) {
		name=n;
	}
	
	/**
	 * getter of the attribute Score
	 * 
	 * @return an Integer representing the quiz
	 */
	public int getScore() {
		return this.score;
	}
	
	/**
	 * Setter fo the attribute Score
	 * 
	 * @param s an Integer representing the score of the user  
	 */
	public void  setScore(int s) {
		score=s;
	}
	
}
