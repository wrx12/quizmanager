package fr.epita.fundamentalProject.data;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ThreadLocalRandom;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * 
 * <p>This class represents a Quiz .</p>
 *      <p>		1. It generates a random  quiz based on a chosen level</p>
 *      <p>		2. it has a method which correct the assignment and show it </p>
 *      <p>		3. it has a method to retrieve the user score after an assignment</p> 
 *      <p>		4. it has a method which assemble all the generated quiz into one 
 *      string to pass it to a PDF object to export in a plain-text format</p> 
 * 
 * @author Emmanuel Bentum
 * @version 1.0
 * @since 1.0
 *
 */
public class Quiz {

	/*############################################################################################################################################################################
	 * 																ATTRIBUTES
	 * ###########################################################################################################################################################################
	 */
	
	
	private Object obj ;
	private String level;
	
	private JSONParser parser = new JSONParser();
	private ObjectMapper mapper = new ObjectMapper();
	
	private Scanner read = new Scanner(System.in);
	
	private int i=1,score=0,range=0;
	private final int  numberOfQuestionForQuiz=10;
	private int objectId=0;
	
	private JSONObject answerObject;
	private JSONObject dataObject;
	private JSONArray reponsesArray;
	private JSONArray objectArray;
	private JSONArray data;
	private JSONObject jsonObjectInMainArray;
	
	 private String mapFromJson;
	 private String questions;
	 private String reponses;
	 private String reponsesSansCaractere ;
	 private String userChoice;
	 private String[] questionReponse;
	 private StringBuilder plainTextformat = new StringBuilder();
	 
	 private Map<String, Object> jsonMap = new HashMap<String, Object>();
	 private List<String> correctAnswerList = new ArrayList<String>();
	 private List<String> userInputList = new ArrayList<String>();
	 private Set<Integer> idOfQuestions = new HashSet<Integer>();
	 
	 private ListIterator<String>litr;
	
	 
	/*############################################################################################################################################################################
	 * 																		Constructor
	 * ###########################################################################################################################################################################
	 */
	 /**
	  * is the constructor to instantiate a quiz object
	  * 
	  * @param topic :
	  *					a string that is the Topic chosen by the user in which he wants to have the quiz
	  * @param level :
	  * 				a string expressing the level of difficulty of the quiz 
	  * @param plain :
	  * 				a string builer argument containing a welcome message and personal info of the user 
	  * 				having the quiz and will be filled progressively to finally convert it to a String type 
	  * 				to pass it as argument to PDF object to have it in plain-Text format.
	  * 
	  * 
	  * 
	  *
	  * 
	  * 
	  */
	 //@exception FileNotFoundException if the file containg the questions and  answers is not found 
	public Quiz (int topic,String level,StringBuilder plain) {
		
				this.level=level;
				this.range=10;
				this.plainTextformat=plain;
		
			 try {
				 
				 if (topic == 1) 
				 {
					 	obj =parser.parse(new FileReader("java.json"));
					 	if (level.equals("easy"))
					 		this.range=30;
					 	else if (level.equals("medium"))
					 		this.range=25;
					 	else
					 		this.range=20;
				 }
				 else if (topic == 2) 
				 {
					 	obj = parser.parse(new FileReader("logic.json"));
					 	
					 	
				 }
				 else 
				 {
					 	obj = parser.parse(new FileReader("geography.json"));
					
				 }
				
			 }catch (FileNotFoundException e) {
				System.out.println("File not Found --> " + e.getMessage()+"\n");
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("Path not Found --> " + e.getMessage()+"\n");
				e.printStackTrace();
			} catch (ParseException e) {
				System.out.println("Parse not Possible --> " + e.getMessage()+"\n");
				e.printStackTrace();
			}
		}
	/*################################################################################################################################################################################
	 * 																			METHODS
	 * ############################################################################################################################################################################
	 */
	
	
	/**
	 * <p> This is the main method which is the quiz in itself.
	 * Obviously it launches the quiz. </p>
	 * <p>what is does inside: 
	 * 		 Read and explore the appropriate JSON File containing all the questions and Answers
	 * 		 create an appropriate Quiz and Correct it by using another private method  below
	 * 		'makeCorrection' to correct the quiz.</p>
	 * 		
	 */
	
	public void launch() {
	
		objectArray =(JSONArray)obj;
		
		
		@SuppressWarnings("unchecked")
		Iterator <JSONObject>iteratorObjectOfMainArray=objectArray.iterator();
		
		
		while (iteratorObjectOfMainArray.hasNext()) // loop all the objects in the main array
			
		{
			
					// retrieve the objects inside the main array of Objects
					 jsonObjectInMainArray = iteratorObjectOfMainArray.next();
					
					
					/*
					 * Since we have different levels ,
					 *  we will load  the correct object based on the level
					 * 
					 */
					
			if ( jsonObjectInMainArray.get("difficulty").equals(level)) {
				
					
					// lit section Data
					 data =  (JSONArray)jsonObjectInMainArray.get("Data");
					
						
					@SuppressWarnings("unchecked")
					Iterator <JSONObject>iteratorData = data.iterator();
					
					
							 
					//1. a set of random Id. based on that we have a auto-generated random quiz if the topic is Java since we have 20+ questions
					if (jsonObjectInMainArray.get("Topic").equals("Java"))
					 idOfQuestions = generateRandomNumber(range,numberOfQuestionForQuiz);
					else {
						for (int j=1;j<=10;j++) {
							idOfQuestions.add(j);
						}
					}
						
				
							
							 
							 
					 //loop  all the objects of Data 
					 while(iteratorData.hasNext()) {
								
								// retrieve the next Object 
								 dataObject = iteratorData.next();  
									 
									 /*
									  *  generate a quiz , by randomize the questions by Id  
									  */
									 
								 //2. retrieve the Id of the actual Object
								  objectId = (int)(long)dataObject.get("id");
									
								  /*
								   * 3. check if the id of the object is in the set
								   */
								  if ( idOfQuestions.contains(objectId)) {
										  
											 // retrieve the question of that object
											 questions = (String) dataObject.get("Question"); 
											
											System.out.println("Question " + i);
											// add to the string builder to get a plain format text
											plainTextformat.append("Question " + i+"\n");
											System.out.println( "==============================================================================================\n" 
											+ questions + " \n\n What's the correct Answer : \n");
											
											plainTextformat.append("==========================================================================\n" 
											+ questions + " \n\n What's the correct Answer : \n");
											
											/* 
											 * since the answers are Arrays of JSonObject,
											 *  retrieve the Answers, which is Array, of one Object
											 */
											 reponsesArray = (JSONArray) dataObject.get("Answer"); 
											
											// create another iterator for the answers
											@SuppressWarnings("unchecked")
											Iterator <JSONObject> iteratorAnswers  = reponsesArray.iterator();
										
										
											while(iteratorAnswers.hasNext())
											{
														//get  answer Object within Answer Array
														answerObject = iteratorAnswers.next(); 
														
														/*
														 *  Since we have the answer in a set form( [...]) ,
														 *  we want to extract the answer(Keys)  without
														 *  the brackets []
														 */
														 reponses = answerObject.keySet().toString();
														 reponsesSansCaractere = reponses.substring(1, reponses.length()-1);
														 
												            // convert JSON string to Map
												            try {
																jsonMap = mapper.readValue(answerObject.toString(),
																        new TypeReference<Map<String, String>>(){});
															} catch (JsonParseException e) {
																System.out.println("JSON Parse not Possible --> " + e.getMessage()+"\n");
																e.printStackTrace();
															} catch (JsonMappingException e) {
																System.out.println("JSON Mapping not Possible --> " + e.getMessage()+"\n");
																e.printStackTrace();
															} catch (IOException e) {
																System.out.println("Path not Found --> " + e.getMessage()+"\n");
																e.printStackTrace();
															}
												            
												            
												            /*
												             * find the correct answer and save it 
												             * in a List 
												             */
												            
												            //1. extraction of the map in this format : key=value
												             mapFromJson = jsonMap.toString().substring(1, jsonMap.toString().length()-1);
												             
												             // 2. split to extract the correct answer
												             questionReponse=mapFromJson.split("=");
												             
												             /* 
												              * 3. retrieve the answer(questionResponse[0]) and save it in the list
												              * to compare it with the user input to check if correct or not
												              */
												 
												             	if ( questionReponse[1].equals("true")) {
												             		correctAnswerList.add(questionReponse[0].substring(1,2));
												             	}
												
															System.out.println("***** " + reponsesSansCaractere + " *****> \n\n");
															
															plainTextformat.append("***** " + reponsesSansCaractere + " ***** \n\n");
														
											}
											
											
											
											// Iterate  the list of Answers
											
											litr = correctAnswerList.listIterator();
											
											
											// read user answer
											System.out.print(" Your answer ");
											
											do {
												// read user answer
												System.out.print(" (either A or B or ...)==> ");
												 userChoice = read.nextLine();
											}while(!userChoice.toLowerCase().equals("a") && !userChoice.toLowerCase().equals("b") &&
													!userChoice.toLowerCase().equals("c") &&!userChoice.toLowerCase().equals("d") &&
													!userChoice.toLowerCase().equals("e") &&!userChoice.toLowerCase().equals("f") );
											
											plainTextformat.append("Your answer : " + userChoice);
											// add the user input in a list  for further comparison
											userInputList.add(userChoice.toUpperCase());
											
											System.out.println("\n");
													
											System.out.println( "==============================================================================================\n");
											plainTextformat.append("\n==========================================================================\n\n");
											
											i++;  // number of question i.e question 1 , question 2, ...
									 } //  end of If 
								
					} // end of main loop
					 
					score = makeCorrection(correctAnswerList,userInputList);
							
					System.out.println("\nQuiz Ended!");
					
					
							
					
			}// end of verification of if for the level checking
			
		} // end of iteration of main array of object
		
	} // closing of 'launch' method
	
	
	
	//############################################################################################################################################################
	
	// generate random numbers within a range so that we can generate a random quiz
	static private Set<Integer> generateRandomNumber(int limitRange, int n) {
		
		 Set<Integer> set = new HashSet<Integer>();
		 int x=0;
		
		for (int i=0; i<n ; i++)
		{	
			//   random number with range(1,limit)
			do {
				x=ThreadLocalRandom.current().nextInt(1,limitRange);
			}while ( set.contains(x));  // check if the number already exist in the set
			
			set.add( x);
		}
		/*
		 * the Set could be unsorted,so we sort it,
		 * to better explore the JSON Object
		 */
		
		TreeSet<Integer> sortedSet = new TreeSet<Integer>(set);
		return sortedSet;
		
	}
	
	//############################################################################################################################################################
	/**
	 * 
	 * @return an Integer representing the user Score
	 */
	public int getScore() {
		return score;
	}


	//############################################################################################################################################################
	
	// Do the correction of the quiz
	static private int  makeCorrection(List<String> corro, List<String>userInput) {
		int score =0;
		String x1;
		String x2;
		
		ListIterator<String>litr1=corro.listIterator();
		ListIterator<String>litr2=userInput.listIterator();
		
		while(litr1.hasNext() && litr2.hasNext() ) {
			
			x1=litr1.next(); x2=litr2.next();
			if ( x1.equals(x2)) {
				score++;
			}
		}
		return score;
	}
	
	
	
//############################################################################################################################################################
/**
 *    It shows what the user should have replied during the quiz
 */
	public void showCorrection() {
		System.out.println("This is what you should have replied : ");
		
		for (int i=0; i<10;i++) {
			System.out.println(i+1 +" : "+correctAnswerList.get(i));
		
		}
		
	}
	
//############################################################################################################################################################
	
	
	/**
	 * 	converts the stringbuilder argument passed during instanciation, and filling up as the questions was generating
	 *  into a String to have a final String format which is the total quiz in itself   
	 * 
	 * @return a String representing the generated Quiz 
	 */
	public String retrievePlainText() {
		
		plainTextformat.append("This is what you should have replied : \n");
		for (int i=0; i<10;i++) {
			
			plainTextformat.append(i+1 +" : "+correctAnswerList.get(i)+"\n");
		}
		plainTextformat.append("\nScore = "+getScore()+"/10");
		
		return plainTextformat.toString();
	}
	
}



