package fr.epita.fundamentalProject.data;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import fr.epita.fundamentalProject.launcher.Main;

/**
 * this class generate a pdf file based on some information it receives 
 * 
 * @author Emmanuel Bentum
 * @version 1.0
 * @since 1.0
 *
 */

public class Pdf {
	
	private String text;
	private Document document;
	FileOutputStream file;
	
	public Pdf (String text) {
		
		this.text=text;
		 document = new Document();
	}
	
	/**
	 *  This method creates a PDF file and adds as content the text of the argument passed during instantiation.
	 *  the created file is called quiz.pdf and it's saved in your personal directory 
	 *  
	 */
	public void exportToPdf() {
		
		try
	      {
			if ( Main.OS.equals("linux")) {
				file = new FileOutputStream(Main.homeDirectory.concat("/quiz.pdf"));
				System.out.println("file quiz.pdf created in your home directory("+Main.homeDirectory+"/quiz.pdf)");
			}
			else if (Main.OS.equals("windows")) {
				file = new FileOutputStream(Main.homeDirectory.concat("\\quiz.pdf"));
				System.out.println("file quiz.pdf created in your home directory("+Main.homeDirectory+"\\quiz.pdf)");
			}
			else
				file = new FileOutputStream("quiz.pdf");
			
	         PdfWriter writer = PdfWriter.getInstance(document, file);
	         document.open();
	         
	         document.add(new Paragraph(text));
	         document.close();
	         
	         writer.close();
	      } catch (DocumentException e)
	      {
	         e.printStackTrace();
	      } catch (FileNotFoundException e)
	      {
	         e.printStackTrace();
	      }
	}
	
	

}
